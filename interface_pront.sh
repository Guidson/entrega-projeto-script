#!/bin/bash

#Projeto de Script - Instituto Federal da Paraíba
#Graduação em Redes de Computadores

#Pacotes necessários para rodar o script:
#	pdftk
#	yad
#	evince

#Grupo:
#	Cleydson
#	Icaro
#	Guidson
#	Ricardo

#Interface inicial contendo as 4 principais opções do script
#--title 	= titulo da janela
#--center	= centraliza a janela
#--width	= largura da janela
#--button	= tipo do campo na janela
yad --center \
	--width 500 --title "Editor de pdf" \
	--button="Unir dois pdfs" \
	--button="Dividir por páginas" \
	--button="Unir páginas soltas" \
	--button="Exluir páginas"

#Variável que guarda o return do menu
#0	= Unir
#1	= Divir
#2	= Unir páginas
#3	= Exluir páginas
#PS: O comando $? é uma variável global do Linux e guarda a saída do último comando
answear=$?

#Case que analisa e direciona o usuário a função solicitada
case "$answear" in

       "0")
	
	#Primeiro arquivo pdf a ser selecionado para a união
	file1=$(

		#Interface de escolha de arquivo para a união
		#--height	= Altura da janela
		#--file		= Opção do YAD que permite escolher um arquivo da máquina do usuário
		yad --center --width 500 --height 500 --title "Editor de pdf - Primeiro Arquivo" \
			--file)

	#Segundo arquivo pdf a ser selecionado para a união
	file2=$(

		#Segunda interface de escolha de arquivo para a união
		yad --center --width 500 --height 500 --title "Editor de pdf - Segundo Arquivo" \
			--file)

	#Nome do novo pdf que será criado após a união dos outros dois pdfs
	newFile=$(

		#Interface para escolha do nome do novo arquivo pdf
		#--entry		= Opção que indica um balão de texto para o usuário digitar, a "entrada"
		#--text-align=center	= Alinhamento do texto = centro
		#--text			= Texto que explica o campo
		#--entry-text		= Texto já inserido no campo como entrada padrão
		yad --center --width 500  --title "Editor de pdf - Unir PDFs" \
	       		--entry --text-align=center --text "Nomeie o novo pdf:" \
			--entry-text "novo.pdf" )
	
	#PDFTK - Usado aqui para fazer um merge entre dois arquivos pdfs selecionados anteriormente e criar um novo arquivo nomeado anteriormente
	# &> /dev/null	= Enviam a saída padrão e a saída padrão de erro para /dev/null, evitando que o usuário veja qualquer mensagem no terminal
	pdftk A=$file1 B=$file2 cat A B output $newFile &> /dev/null

	#Interface com a mensagem de sucesso após a criação do novo arquivo pdf pós merge
	#PS: A mensagem aparece com ou sem sucesso! (Se pensar em uma forma de garantir que só apareça caso haja sucesso sinta-se livre para editar!)
	yad --center --title "Editor de pdf" --text-align=center --text "PDFs unidos com sucesso!"

	#Evince - Leitor de pdf que pode ser usado via terminal
	#-w (ou --preview)	= Exibe um preview do arquivo
	evince -w "$newFile" &> /dev/null
	;;

	
	"1")
	
	#Arquivo a ser fragmentado
	fileS=$(
		
		#Interface para selecionar o arquivo pdf a ser fragmentado
		yad --center --width 500 --height 500 --title "Editor de pdf - Dividir PDFs" \
			--file)
	
	#PDFTK - Usado aqui para fragmentar o pdf selecionado anteriormente em vários outros pdfs. Nomeados de forma padrão como "pg_000X.pdf"
	pdftk "$fileS" burst &> /dev/null
	yad --center --title "Editor de pdf" --text-align=center --text "PDF dividido com sucesso!"

	#pg_00*.pdf	= Usado para selecionar todos os arquivos salvos como "pg_00(qualquer coisa).pdf"
	evince pg_00*.pdf &> /dev/null
	;;
	
	"2")
	
	#Nome do arquivo pdf que será criado a partir das páginas fragmentadas presentes no diretório
	rebirth=$(

		#Interface para escolher o nome do arquivo pdf que será gerado a partir dos fragmentos presentes no diretório
		yad --center --width 500 --title "Editor de pdf - Unir páginas" \
			--entry --text-align=center --text "Informe o nome do pdf a ser criado a partir das páginas avulsas divididas: ")

	#PDFTK - Usado aqui para juntar todas as páginas no diretório com "pg_(qualquer coisa).pdf" e criar o arquivo pdf $rebirth
	pdftk pg_*.pdf cat output $rebirth &> /dev/null
	yad --center --title "Editor de pdf" --text-align=center --text "PDF criado com sucesso!"

	#Comando para excluir as páginas já unidas na criação do arquivo $rebirth
	rm pg_*.pdf
	evince -w $rebirth &> /dev/null
	;;

	"3")

	#Arquivo pdf do qual será excluída a página
	fileD=$(
		yad --center --width 500 --height 500 --title "Editor de pdf - Deletar página" \
			--file)

	#Página a ser excluída do arquivo
	page=$(
		yad --center --width 500 --title "Editor de pdf - Página a deletar" \
			--entry --text-align=center --text "Escolha a página do pdf que deseja excluir: " )

	#Novo nome do arquivo pós exclusão da página
	fname=$(
		yad --center --width 500 --title "Editor de pdf - Deletar página" \
		       --entry --text-align=center --text "Nomeie o novo pdf: " \
		       --entry-text "novo.pdf")

	#Condição para avaliar se a página a ser excluída vai ser a primeira do arquivo ou não
	if [ $page -eq 1  ] ;then

		#PDFTK - Aqui juntará todas as páginas do arquivo selecionado, começando da segunda até o fim, e criará o arquivo renomeado sem essa página
		pdftk $fileD cat 2-end output $fname &> /dev/null
	else
		#PDFTK - Aqui excluí a página juntando todas as páginas até a anterior a página indicada e pós a mesma
		pdftk $fileD cat 1-$(($page - 1)) $(($page + 1))-end output $fname &> /dev/null
	fi
	yad --center --title "Editor de pdf" --text-align=center --text "Página exluída com sucesso!"
	evince -w $fname &> /dev/null
	;;

esac
